��    	      d      �       �   `   �      B     R     d     u     �  
   �     �  �  �  �   v  Z         y  !   �  >   �  C   �     ?     N                         	                     Paste a feed URL or enter a web address like <i>nytimes.com</i> to search for its main news feed enable feedLogo enable glancrNews invalid feed url news feed url input news_description news_title validate Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-23 19:02+0100
PO-Revision-Date: 2018-08-16 10:19+0800
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: 
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Вставьте URL-адрес канала или введите веб-адрес, например <i>nytimes.com</i> искать основную новость  показать логотип сайта в конце каждого заголовка показать новости  неверный URL канала Вход для канала новостей новостей Последние новости от новостных лент. Новости подтвердить 