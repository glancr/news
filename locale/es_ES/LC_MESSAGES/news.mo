��    	      d      �       �   `   �      B     R     d     u     �  
   �     �  �  �  j   x  :   �          9  *   P  <   {     �  
   �                         	                     Paste a feed URL or enter a web address like <i>nytimes.com</i> to search for its main news feed enable feedLogo enable glancrNews invalid feed url news feed url input news_description news_title validate Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-23 19:03+0100
PO-Revision-Date: 2018-09-04 14:45+0200
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Inserte una URL de feed o escriba una dirección web como <i>spiegel.de</i> para buscar el feed principal. mostrar el logotipo del sitio web después de cada titular Mostrar noticias de glancr URL de feed no válida Introduzca la URL de la fuente de noticias Muestra las entradas más recientes de un canal de noticias. Noticias convalidar 