��    	      d      �       �   `   �      B     R     d     u     �  
   �     �  �  �  l   x  1   �          (  "   =  >   `  	   �     �                         	                     Paste a feed URL or enter a web address like <i>nytimes.com</i> to search for its main news feed enable feedLogo enable glancrNews invalid feed url news feed url input news_description news_title validate Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-23 19:03+0100
PO-Revision-Date: 2018-09-04 14:51+0200
Last-Translator: Gordon Böhme <gb@glancr.de>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Insérez une URL de flux ou tapez une adresse web comme <i>spiegel.de</i> pour rechercher le flux principal. afficher le logo du site Web après chaque titre. show glancr-News URL de flux invalide Entrez l'URL du flux d'actualités Affiche les entrées les plus récentes d'un fil de nouvelles. Nouvelles valider 